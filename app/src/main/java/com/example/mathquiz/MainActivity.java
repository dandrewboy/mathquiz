package com.example.mathquiz;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button b_start, b_answer0, b_answer1, b_answer2, b_answer3;
    TextView tv_timer, tv_points, tv_question, tv_answer;
    ProgressBar pb_timer;

    Game g = new Game();

    int secondsRemaining = 30;

    CountDownTimer timer = new CountDownTimer(30000, 1000) {
        @Override
        public void onTick(long millisUntilFinished) {
            secondsRemaining--;
            tv_timer.setText(Integer.toString(secondsRemaining) + "sec");
            pb_timer.setProgress(30 - secondsRemaining);
        }

        @Override
        public void onFinish() {
            b_answer0.setEnabled(false);
            b_answer1.setEnabled(false);
            b_answer2.setEnabled(false);
            b_answer3.setEnabled(false);
            tv_answer.setText("Times up! You got " + g.getNumberCorrect() + "/" + (g.getTotalQuestions() - 1) + " Correct!");

            final Handler handler = new Handler();

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    b_start.setVisibility(View.VISIBLE);

                }
            }, 4000);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        b_start = findViewById(R.id.b_start);
        b_answer0 = findViewById(R.id.b_answer0);
        b_answer1 = findViewById(R.id.b_answer1);
        b_answer2 = findViewById(R.id.b_answer2);
        b_answer3 = findViewById(R.id.b_answer3);

        tv_timer = findViewById(R.id.tv_timer);
        tv_points = findViewById(R.id.tv_points);
        tv_question = findViewById(R.id.tv_question);
        tv_answer = findViewById(R.id.tv_answer);

        tv_timer.setText("0 Sec");
        tv_question.setText("");
        tv_points.setText("0 points");
        tv_answer.setText("Press Go!");
        b_start.setText("Go!");
        b_answer0.setText("");
        b_answer1.setText("");
        b_answer2.setText("");
        b_answer3.setText("");

        pb_timer = findViewById(R.id.pb_timer);



        b_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button start_button = (Button) v;
                start_button.setVisibility(View.INVISIBLE);
                secondsRemaining = 30;
                g = new Game();
                nextTurn();
                timer.start();
            }
        });

        View.OnClickListener answerClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button answer_button = (Button) v;
                int answerSelected = Integer.parseInt(answer_button.getText().toString());
                g.checkAnswer(answerSelected);
                tv_points.setText(Integer.toString(g.getScore()));
                nextTurn();


            }
        };
        b_answer0.setOnClickListener(answerClickListener);
        b_answer1.setOnClickListener(answerClickListener);
        b_answer2.setOnClickListener(answerClickListener);
        b_answer3.setOnClickListener(answerClickListener);



        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void nextTurn() {

        g.makeNewQuestion();
        int [] answer = g.getCurrentQuestion().getAnswerArray();

        b_answer0.setText(Integer.toString(answer[0]));
        b_answer1.setText(Integer.toString(answer[1]));
        b_answer2.setText(Integer.toString(answer[2]));
        b_answer3.setText(Integer.toString(answer[3]));

        b_answer0.setEnabled(true);
        b_answer1.setEnabled(true);
        b_answer2.setEnabled(true);
        b_answer3.setEnabled(true);

        tv_question.setText(g.getCurrentQuestion().getQuestionPhrase());

        tv_answer.setText(g.getNumberCorrect() + "/" + (g.getTotalQuestions() - 1));



    }
}
